======================
 Asynchronous Modules
======================

API
===

module
------

Defines a module with optional dependencies. ::

  module('name', ['dep1', 'dep2'], function(dep1, dep2){...})

  module('name', function(){...})

Without a name argument, the module name defaults to the path of the module's
containing file ::

  module(['dep1', 'dep2'], function(dep1, dep2){...})

  module(function(){...})

require
-------

Runs a callback after loading dependencies without defining a module. ::

   require(['dep1', 'dep2'], function(dep1, dep2){...})

provide
-------

Specifies the source file to load for a module name instead of using the
module's name as a path. ::

  provide('backbone', 'http://cdn...backbone.js')

Does not load the dependency when provide is called unless an attempt has
already been made to load that module. Otherwise it will load the first time
it's declared as a dependency or required.

load_dependency
---------------

Forces a dependency to load immediately. Given the `provide` example above, we
could force backbone to load via ::

  load_dependency('backbone')

or give it a path ::

  load_dependency('mystuff/utils.js')
