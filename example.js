/*global provide, module, require */

// don't load underscore right now, but if anybody tries to import or declare
// 'underscore' as a dependency, load it from this url.
provide('underscore',
        'https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js');
provide('jquery',
        'http://code.jquery.com/jquery-1.11.2.min.js');

// module('cheesecake', ['example.js'], function(example){
//     this.x = 1;
// });

module('cheesecake', ['utils'], function(utils){
});

// ad hoc module with optional name argument. will be example as 'utils'
// module will be available at example.js, based on its file's path.
module(['utils', 'cheesecake', 'pooncake'], function(a, b,c){
    console.log(a, b, c);
});

module('utils', [], function(){
    this.log = function(x){ console.log(x); };
    this.log('32');
});

module('beefcake', ['cheesecake'], function(c){
    console.log(c);
});
module('pooncake', ['beefcake'], function(){
});
