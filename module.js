// An object with module names as keys and promises for module objects as
// values. register() is the only function that modifies this object.
module.registry = {};
// module.installers['mymod'] is a function that takes a module object and
// installs it in the registry for 'mymod'.
module.installers = {};
module.rejectors = {};

module.declared = [];
module.installed = [];

var debug = true;

function make_installer(name){
    return new Promise(function(resolve, reject){
        module.rejectors[name] = function(mod){
            module.registry[name] = Promise.reject(mod);
            reject(mod);
        };
        module.installers[name] = function(mod){
            module.registry[name] = Promise.resolve(mod);
            resolve(mod);
        };
    });
}

function declare(name){
    if(member(module.declared, name)) {
        throw Error('Attempted to redeclare ' + name);
    }
    module.declared.push(name);
    if(debug) console.debug('declared  ' + name);
}

// Installs a module object in the global registry.
function install(name, mod){
    if(debug) console.debug('installed ' + name, mod);
    var installer = module.installers[name];
    if(installer) {
        installer(mod);
    } else {
        module.registry[name] = Promise.resolve(mod);
    }
    module.installed.push(name);
}

function reject(name, e){
    module.rejectors[name](e);
}

// returns a newly created promise for the requested module
function request(name){
    if(debug) console.debug('requested ' + name);
    return module.registry[name] || make_installer(name);
}

function installed(name){
    return member(module.installed, name);
}
// returns true if module has been requested as a dependency.
function requested(name){
    return !!module.installers[name];
}

// A mapping between module names and the names of their dependencies. Used for
// dependency cycle detection. Updated by module() calls.
module.dependency_graph = {};

/*
 `name` is an optional string module name. defaults to the name of the script
 the module is defined in. this is the identifier used when other modules
 specify it as a dependency.

 `dependencies` is a an optional list of strings, each one a module name, that
 this module depends on. If the dependencies array is omitted, the argument
 names in `body` will be used as module names.

 `body` is a function to be executed as soon as all dependencies are
 loaded. It's called with the module objects specified by the `dependencies`
 list as its arguments, in the order they occured in that list. The `this`
 variable is bound to the module object defined by the module call.

 For example:

     module('c', function(a, b){
       // when this function executes, a and b are module objects that have been loaded.
       // `this` refers to module c.
       this.import(function(c){
         ...
       });
     });

*/
function module(name, dependencies, body){
    // module can also be called with 1 or 2 arguments. the one-argument case is
    // where only a callback is passed in, the two-argument case either a
    // dependency list and a callback or a name and a callback.

    // capture dynamic vars before any callbacks
    var path = __get_script_name(),
        script_loading = !!path;

    if(arguments.length == 1){
        // module(function(){ ... });
        body = name;
        name = path;
        dependencies = [];
    } else if(arguments.length == 2){
        body = dependencies;
        if(typeof name !== 'string'){
            // called as module([deps], fn)
            dependencies = name;
            name = path;
        } else {
            // called as module('name', fn)
            dependencies = [];
        }
    }

    var mod = {
        _meta: {
            name: name, dependencies: dependencies, path: path
        }
    };

    if (!name){
        var msg = 'Module name not defined- did you try to make an auto-named module in a callback?';
        throw new Error(msg);
    }

    dependencies = resolve_relative_deps(mod, dependencies);
    // throw error on cyclic module dependency
    module.dependency_graph[name] = dependencies;
    var cycle = detect_cycle(module.dependency_graph);
    if(cycle){
        msg = 'Cyclic dependency between ' + cycle.join(' and ');
        throw new Error(msg);
    }

    declare(name);
    return require.call(mod, dependencies, body)
        .then(function(){
            install(name, mod);
            return mod;
        }).catch(function(e){
            // TODO: is there a way to reject the promise but allow later
            // attempts to try again? perhaps broadcast a failed attempt but
            // keep the promise pending?
            // reject(name, e);
            return Promise.reject(e);
        });
}

// resolves relative module references (relative to module obj `mod`) to
// absolute ones.
function resolve_relative_deps(mod, deps){
    return deps.map(function(dep){
        if(dep.slice(0,2) === './'){
            return mod._meta.name + '.' + dep.slice(2);
        }
        return dep;
    });
}

// Run callback with module objects specified by `dependencies` array once
// dependencies are loaded. Effectively an import statement that runs a callback
// with the imported modules.
function require(dependencies, callback){
    var that = this;
    return load_all(dependencies).then(function(deps){
        return callback.apply(that, deps);
    }).catch(function(e){ return Promise.reject(e); });
}

// Returns a promise that resolves when all dependencies have resolved
function load_all(dependencies){
    return Promise.all(dependencies.map(load_dependency));
}

// injects a script on page. returns a promise that resolves on success, rejects
// on error.
function inject_script(path){
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = path;
    document.head.appendChild(script);
    return new Promise(function(resolve, reject){
        script.onload = resolve;
        script.onerror = reject;
    });
}

provide.providers = {};
// declares a source for a module name, but doesn't load it unless it's already
// been requested, otherwise will load the first time it's requested.
function provide(name, path){
    provide.providers[name] = path;
    if(requested(name)) load_dependency(name);
}

function provided(name){
    return !!provide.providers[name];
}

// TODO: perhaps the promise to load a dependency should be separate from the
// promise for the dependency itself. this way a subscriber can decide to care
// about this particular loading attempt or the ultimate resolution of a
// dependency. perhaps load_dependency should return a Promise<Promise<module>>
function load_dependency(name){
    var reg = module.registry,
        path = provide.providers[name] || name;
    if(reg[name]) return reg[name];

    var promiseForModule = request(name);

    get(path).then(function(code){
        var old_globals = global_vars(), new_globals;

        define.amd = true;
        function define(external_name, deps, fn){
            install(name, fn());
        };

        __currentScript = path; // "dynamic" variable
        try {
            new_globals = eval_code_get_globals(code, {define: define})
                .filter(function(p){
                    return !member(old_globals, p);
                });
        } catch(e){
            return Promise.reject(e);
        } finally {
            __currentScript = null;
        }

        var mod;
        if(new_globals.length == 1 && typeof window[new_globals[0]] === 'object'){
            mod = window[new_globals[0]];
            mod._meta = {name: name, path: path, dependencies: []};
            install(name, mod);
            delete window[new_globals[0]];
        }
        if(new_globals.length > 1){
            mod = {_meta: {name: name, path: path, dependencies: []}};
            for(var i=0; i < new_globals.length; i++){
                var prop = new_globals[i];
                mod[prop] = window[prop];
                delete window[new_globals[i]];
            }
            install(name, mod);
        }
    }, function(){
        // module is either not file-based, or missing.
    });

    return promiseForModule;
}

// *********************
// * UTILITY FUNCTIONS *
// *********************

function get(url){
    var req = new XMLHttpRequest();
    req.open('get', url);
    return new Promise(function(resolve, reject){
        req.onreadystatechange = function(){
            if(req.readyState == 4){
                if(req.status == 200){
                    resolve(req.response);
                } else {
                    reject(req.status);
                }
            }
        };
        req.send();
    });
}

// returns all global vars currently defined
function global_vars(){
    var globals = [];
    for (var prop in window){
        globals.push(prop);
    }
    return globals;
};

// evals code inside with(scope), returning all global vars defined after
// evaluation is complete.
function eval_code_get_globals(code, scope){
    with(scope){
        eval(code);
    }
    return global_vars();
}

// taken from http://stackoverflow.com/a/9924463
var STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;
var ARGUMENT_NAMES = /([^\s,]+)/g;
function getParamNames(func) {
  var fnStr = func.toString().replace(STRIP_COMMENTS, '');
  var result = fnStr.slice(fnStr.indexOf('(')+1, fnStr.indexOf(')')).match(ARGUMENT_NAMES);
  if(result === null)
     result = [];
  return result;
}

function member(array, element){
    return array.indexOf(element) > -1;
}

// returns the first cyclic dependency as an array of two strings, representing
// the edge that creates the cycle. `graph` is an object that represents graph
// transitions, each key a module name, and each value and array of strings, the
// modules depended on.
function detect_cycle(graph){
    function dc(node, ancestors){
        if (typeof ancestors === 'undefined') ancestors = [];
        if (member(ancestors, node)) return [node, ancestors.slice(-1)[0]];
        var children = graph[node] || [];
        return children.reduce(function(acc, child){
            return acc || dc(child, ancestors.concat([node]));
        }, false);
    }
    return Object.keys(graph)
        .map(function(x){ return dc(x); })
        .filter(function(x){ return !!x; })[0];
}

var __currentScript;
function __get_script_name(){
    var name = document.script && document.script.src;
    if(name)                  return name.slice(window.location.href.length);
    else if (__currentScript) return __currentScript;
    else                      return null;
}

function synchronous_loading(script){
    return script && !script.async;
}
