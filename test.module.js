/*global beforeEach, afterEach, describe, it, expect, module, install, member, provide */
var expect = chai.expect;
function reset(){
    module.registry = {};
    module.installers = {};
    module.installed = [];
    module.declared = [];
    provide.providers = {};
}
beforeEach(reset);

function identity(x){ return x; }
function constant(x){ return function(){ return x; }; }

function testModule(name, deps, path){
    return {
        _meta: {
            name: name, path: path || null, dependencies: deps || []
        }
    };
}

describe('Installing a module', function(){
    it('should add that module to the installed array', function(){
        var mod = testModule('test'), name = 'test';
        install(name, mod);
        expect(member(module.installed, name)).to.equal(true);
    });

    it('should add that module to the registry', function(){
        var mod = testModule('test'), name = 'test';
        install(name, mod);
        expect(typeof module.registry[name]).to.equal('object');
        expect(typeof module.registry[name].then).to.equal('function');
    });

    it('should fire the corresponding installer function', function(){
        var expected = testModule('test'), observed, name = 'test';
        module.installers[name] = function(mod){
            observed = mod;
        };
        expect(observed).to.equal(undefined);
        install(name, expected);
        expect(observed).to.equal(expected);
    });
});

describe('Declaring a module', function(){
    jsc.property('should return a promise for the module', 'string', function(name){
        if(name == "") return true;
        reset();
        var mod, error;
        var p = module(name, [], function(){
            this.magicVal = 400;
            mod = this;
        });
        return p.then(function(obj){
            expect(obj).is.an('object');
            expect(obj).to.equal(mod);
            return true;
        });
    });

    it('should reject if its body errors', function(){
        return expect(module('test', function(){
            throw new Error();
        })).to.eventually.be.rejected;
    });

    it('should be passed a single available dependency', function(){
        var mod = testModule('dep');
        install('dep', mod);
        return expect(module('test', ['dep'], function(dep){
            this.dep = dep;
        })).to.eventually.deep.equal({
            _meta: testModule('test', ['dep'])._meta,
            dep: mod
        });
    });

    it('should be passed multiple available dependencies', function(done){
        var a = testModule('a'), b = testModule('b'), c = testModule('c');
        install('a', a);
        install('b', b);
        install('c', c);
        module('test', ['a', 'b', 'c'], function(dep_a, dep_b, dep_c){
            var m = this._meta;
            expect(this._meta).to.deep.equal({
                name: 'test', path: null, dependencies: ['a', 'b', 'c']
            });
            expect(dep_a).to.equal(a);
            expect(dep_b).to.equal(b);
            expect(dep_c).to.equal(c);
            done();
        });
    });
    it('should resolve dependencies recursively', function(done){
        var a, b, c, counter = 0;
        module('b', ['a'], function(d_a){
            b = this;
            expect(d_a).to.equal(a);
            expect(counter++).to.equal(1);
        });
        module('d', ['c'], function(d_c){
            expect(d_c).to.equal(c);
            expect(counter++).to.equal(3);
            done();
        });
        module('a', [], function(x){
            a = this;
            expect(x).to.equal(undefined);
            expect(counter++).to.equal(0);
        });
        module('c', ['b'], function(d_b){
            c = this;
            expect(d_b).to.equal(b);
            expect(counter++).to.equal(2);
        });
    });

    describe('without a name', function(){
        var _get_script_name, _synchronous_loading, script_module_names;

        it('should throw error when not loaded as dependency', function(){
            expect(function(){ module([], function(){}); })
                .to.throw();
        });

        it('Should reject when their body errors', function(){
            return expect(load_dependency('test-files/unnamed-in-callback.js'))
                .to.eventually.be.rejected;
        });

        it('should set correct metadata on module object', function(){
            var path = 'test-files/simple-module.js';
            return expect(load_dependency(path))
                .to.eventually
                .deep.equal(testModule(path, [], path));
        });

        it('should handle bare-modules with single global var', function(){
            return expect(load_dependency('test-files/bare-module-single.js'))
                .to.eventually.deep.equal({
                    _meta: {
                        name :'test-files/bare-module-single.js',
                        dependencies: [],
                        path: 'test-files/bare-module-single.js'},
                    a: 1, b: 2, c: 3
                });
        });

        it('should handle bare-modules with multiple vars', function(){
            return expect(load_dependency('test-files/bare-module-multiple.js'))
                .to.eventually.deep.equal({
                    _meta: {
                        name :'test-files/bare-module-multiple.js',
                        dependencies: [],
                        path: 'test-files/bare-module-multiple.js'},
                    a: 1, b: 2, c: 3
                });
        });
    });
});
